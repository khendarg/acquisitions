#!/usr/bin/env python

import os
import time
import curses
import random
import argparse
import extract_words

def pick_words(iterable, count=1):
	k = sorted(iterable)[:]
	random.shuffle(k)
	return k[:count]

class ReversibilityTest(object):
	stdscr= None
	delay = 2.0
	count = 4
	wordbank = None
	_run = True

	def __init__(self, **kwargs):
		self.delay = kwargs.get('delay', self.delay)
		self.count = kwargs.get('count', self.count)
		self.wordbank = kwargs.get('wordbank', {})

		if not self.wordbank: raise ValueError('Need a non-empty wordbank')

		self.logfile = kwargs.get('logfile', None)
		if self.logfile is None: raise ValueError('Need to write results somewhere')

	def generate_sequence(self):
		return [pick_words(self.wordbank[word])[0] for word in pick_words(self.wordbank, self.count)]

	def get_shuffled(self, sequence):
		result = list(range(self.count))
		random.shuffle(result)
		return result, [sequence[i] for i in result]

	def run(self, stdscr):
		self.stdscr = stdscr
		while self._run:	
			self.do_round()
			self.stdscr.clear()

	def flash_words(self, words):
		for word in words:
			self.stdscr.clear()
			self.stdscr.addstr(0, 0, word, curses.A_BOLD)
			self.stdscr.refresh()
			time.sleep(self.delay)
		self.stdscr.clear()

	def do_round(self):
		words = self.generate_sequence()
		answer=  words[::-1]
		indices, choices = self.get_shuffled(answer)

		self.flash_words(words)
		#self.stdscr.addstr(0,0, ' '.join(words), curses.A_BOLD)
		self.stdscr.refresh()

		selected = []
		start = time.time()
		while 1:
			self.stdscr.clear()
			for i in range(self.count):
				line = '{}: {}'.format(i, choices[i])
				if i in selected: self.stdscr.addstr(i+2,0, line, curses.A_DIM)
				else: self.stdscr.addstr(i+2,0, line)

			self.stdscr.refresh()
			k = self.stdscr.getkey()
			if k == 'q': 
				self._run = False
				return
			elif k in (curses.KEY_BACKSPACE, 'd', 'x', 's'):
				if selected: selected.pop(-1)
			else:
				try: 
					n = int(k)
					if n in range(self.count) and n not in selected: 
						selected.append(n)
						for i in range(self.count):
							line = '{}: {}'.format(i, choices[i])
							if i in selected: self.stdscr.addstr(i+2,0, line, curses.A_DIM)
							else: self.stdscr.addstr(i+2,0, line)
				except ValueError: pass



				if len(selected) == self.count: break
		elapsed = time.time() - start

		attempt = [indices[i] for i in selected]
		score = self.hamming(attempt, range(self.count))

		timestamp = time.strftime('%Y-%m-%dT%H:%M:%SZ')

		out = '{timestamp}\t{delay}\t{count}\t{score}\t{elapsed:0.3f}\n'.format(timestamp=timestamp, 
			delay=self.delay,
			count=self.count,
			score=score,
			elapsed=elapsed
		)
		self.stdscr.addstr(0,0, '{}/{}'.format(score, self.count))

		self.logfile.write(out)

		self.stdscr.refresh()
		time.sleep(0.5)

	def hamming(self, seq1, seq2): return sum([c1 == c2 for c1, c2 in zip(seq1, seq2)])

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	parser.add_argument('-n', type=int, default=4)
	parser.add_argument('-d', type=float, default=0.5, help='How long to flash each word in the forward order')
	parser.add_argument('--logfile', type=argparse.FileType('a'), default=open('progress_reversibility.log', 'a'))

	args = parser.parse_args()

	with open('{}/raw/objects/language_words.txt'.format(os.environ.get('DFPATH', '.'))) as fh:
		nouns = extract_words.filtfile(fh, parts=['NOUN'])
		#print(nouns)
	#print([pick_words(nouns[word])[0] for word in pick_words(nouns, args.n)])

	rev = ReversibilityTest(delay=args.d, count=args.n, wordbank=nouns, logfile=args.logfile)
	curses.wrapper(rev.run)
